public static void InsertionSort(int[] num) {
	int j;      // the number of items sorted so far
	int key;    // the item to be inserted
	int i;
	for (j = 1; j < num.length; j++) {    // Start with 1 (not 0)
		key = num[j];
		for(i = j - 1; (i >= 0) && (num[i] < key); i--) {
			// Smaller values are moving up
			num[i+1] = num[i];
		}
		num[i+1] = key; // Put the key in its proper location
	}
}

// note that this insertion sort puts the smallest element last (ie sorts in reverse order)

test case 1: num = [], output = [], reason: skip the outer for loop
test case 2: num = [2,2], output = [2,2], reason: run the outer loop once & skip inner loop
test case 3: num = [1,2], output = [2,1], reason: run the outer loop once & inner loop once
test case 4: num = [3,2,1], output = [3,2,1], reason: run the outer loop more than once, each time skipping the inner loop
test case 5: num = [1,2,3], output = [3,2,1], reason: ensure the inner loop does two swaps (overall, sorts)
[more test cases possible ...]

// How many cases at minimum?
Each loop has 3 cases: skip loop, loop once, loop more than once
there's two loops, so 3 x 3 = 9 test cases as an upper bound, but
if the outer loop skips, there is no way to loop the inner loop, so:
Test cases according to loops (outer, inner):
(skip, ___)
(once, skip)
(once, once)
(once, multiple)
(multiple, skip)
(multiple, once)
(multiple, multiple)

We would have 7 test cases in total.

test case N: num = null, output = NPE  (this would be black/gray box ish)

// Black box testing goal:
ensure the method behaves according to specs
// example:
1) some input array, output is the sorted array
2) edge cases (null array, empty array, single element array)

// White box testing goal (regardless of technique):
a) every line is executed at least once,
b) every logic statement can evaluate to true and false
c) every branch is taken at least once (this is the consequence of logic test)

// examples:
1) skip outer loop
2) enter outer loop, skip inner loop
3) enter outer loop, enter inner loop, loop once overall (once out, once in)
4) loop outer loop once, inner loop twice
5) loop outer loop twice, inner loop once each time
....

// Only at the step of picking an expected output would we consider specs in white box testing
// the inputs chosen for white box testing is specifically to ensure certain code blocks execute
// and that through all test cases, all code executes correctly






