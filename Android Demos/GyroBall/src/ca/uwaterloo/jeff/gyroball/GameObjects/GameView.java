/*  Author: Jeff Y Luo
    For ECE 155 - Spring 2015
    University of Waterloo
    Department of Electrical and Computer Engineering */

package ca.uwaterloo.jeff.gyroball.GameObjects;

import ca.uwaterloo.jeff.gyroball.Sensors.GyroscopeEventListener;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class GameView extends View {
	private Context gameActivityContext;
	private int viewWidth;
	private int viewHeight;
	private Handler handler;
	private final long FRAME_RATE = (long) 240.0;
	private Ball ball;
	private GyroscopeEventListener gyroListener;

	public GameView(Context context, AttributeSet attrs)  {
		super(context, attrs);
		gameActivityContext = context;

		handler = new Handler();
		gyroListener = GyroscopeEventListener.getListener(null, null, null);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		Log.d("Brick", "on Size Change w: " + w + " h: " + h);

		viewWidth = w;
		viewHeight = h;

		// drawCircle draws a circle centered on the x and y position provided, hence we
		// shift it back by the radius (60) to set the actual default coordinates of the ball (which is the top left corner of the box containing the ball)
		ball = new Ball(viewWidth/2 - 60, viewHeight/2 - 60, 60, 0, 0, "#CD5C5C");
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		// get the new velocity from the gyroscope
		ball.setXVelocity(Math.round(gyroListener.getY()) / 2);
		ball.setYVelocity(Math.round(gyroListener.getX()) / 2);

		// update ball position based on velocity
		ball.updateBallPosition();

		// if ball exceeds the horizontal walls, reset it back to the wall so it doesn't exceed the wall
		if(ball.getX() <= 0)
		{
			ball.setX(0);
		}
		else if(ball.getX() + ball.getWidth() >= viewWidth)
		{
			ball.setX(viewWidth - ball.getWidth());
		}

		// if ball exceeds the vertical walls, reset it back to the wall so it doesn't exceed the wall
		if(ball.getY() <= 0)
		{
			ball.setY(0);
		}
		else if(ball.getY() + ball.getHeight() >= viewHeight)
		{
			ball.setY(viewHeight - ball.getHeight());
		}

		// drawCircle draws a circle centered around an x,y coordinate
		// We want the top left corner of this circle to be our real reference, so add the radius to shift the actual draw position
		canvas.drawCircle(ball.getX() + ball.getRadius(), ball.getY() + ball.getRadius(), ball.getRadius(), ball.getPaint());

		// queue up the runnable r function after a frame rate delay
		handler.postDelayed(redraw, 1000/FRAME_RATE);	// frame rate needs to be passed in as a time delay in milliseconds
	}

	// a Runnable is a kind of function, specifically whatever is inside run() gets executed by the handler
	private Runnable redraw = new Runnable() {
		@Override
		public void run() {
			// invalidate the view, which wipes everything from the canvas and 
			// automatically calls onDraw() for everything on the screen
			invalidate();
		}
	};
}