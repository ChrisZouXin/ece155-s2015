/*  Author: Jeff Y Luo
    For ECE 155 - Spring 2015
    University of Waterloo
    Department of Electrical and Computer Engineering */

package ca.uwaterloo.jeff.gyroball.GameObjects;

import android.graphics.Color;
import android.graphics.Paint;

public class Ball {
	private int pos_x;
	private int pos_y;
	private int radius;
	private int velocity_x;
	private int velocity_y;
	private String colour;
	private Paint paint;

	public Ball(int init_pos_x, int init_pos_y, int rad, int v_x, int v_y, String c)
	{
		pos_x = init_pos_x;
		pos_y = init_pos_y;
		radius = rad;
		velocity_x = v_x;
		velocity_y = v_y;
		colour = c;
		paint = new Paint();
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor(colour));
	}

	public void setX(int x)
	{
		pos_x = x;
	}
	
	public int getX()
	{
		return pos_x;
	}
	
	public void setY(int y)
	{
		pos_y = y;
	}
	
	public int getY()
	{
		return pos_y;
	}
	
	public int getRadius()
	{
		return radius;
	}
	
	public Paint getPaint()
	{
		return paint;
	}
	
	public int getWidth()
	{
		return radius * 2;
	}
	
	public int getHeight()
	{
		return radius * 2;
	}
	
	public void setXVelocity(int x)
	{
		velocity_x = x;
	}
	
	public void setYVelocity(int y)
	{
		velocity_y = y;
	}
	
	public void updateBallPosition()
	{
		// move the ball by the amount equaled to its velocity
		// velocity is in units of display points / draw
		pos_x += velocity_x;
		pos_y += velocity_y;
	}
	
	public void flipX()
	{
		velocity_x = velocity_x * -1;
	}

	public void flipY()
	{
		velocity_y = velocity_y * -1;
	}
}
