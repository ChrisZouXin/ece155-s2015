/*  Author: Jeff Y Luo
    For ECE 155 - Spring 2015
    University of Waterloo
    Department of Electrical and Computer Engineering */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SimpleEventLoopProgram {

	public static void main(String[] args) throws IOException
	{
		// Set up Event Environment
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		boolean quitProgram = false;	// used to exit the main event loop when it is set to true
		String keyboardInput;		// used to store what is entered by the user, which is this program's form of an "event" input
		String[] inputWords;		// used to store the components of the user's input

		// Run main event loop
		mainloop: while(!quitProgram)	// note that we label this while loop as "mainloop"
		{
			System.out.println("Enter something:");
			
			// wait for an event (in this case, wait until the user enters an entire line of text)
			keyboardInput = br.readLine();

			// figure out what the event is, and do something with the event
			switch(keyboardInput.toCharArray()[0])	// specifically, for this demo the first character of the line of text decides what event it is
			{

			// if the event is PLUS, then add the two numbers provided by the user and display the result
			case '+':
				// preprocess the event:
				// separate the input string into an array of strings by splitting at every space bar
				inputWords = keyboardInput.split(" ");
				// process each of the two strings into integers then call the event handler
				addPrintNumbers(Integer.parseInt(inputWords[1]), Integer.parseInt(inputWords[2]));
				break;

			// if the event is MINUS, then subtract the two numbers provided by the user and display the result
			case '-':
				inputWords = keyboardInput.split(" ");
				subPrintNumbers(Integer.parseInt(inputWords[1]), Integer.parseInt(inputWords[2]));
				break;

			// if the event is QUIT, then quit the main event loop, and thus the main program (since nothing happens after the while loop)
			case 'q':
				System.out.println("first way of quitting");
				quitProgram = true;	// set while loop condition so it stops looping
				break;
			case 'Q':
				System.out.println("second way of quitting");
				break mainloop;	// use break syntax, targeting the while loop via the loop label

			// if user enters anything else, prompt them to enter something
			default:
				//System.out.println("enter something:");
			}
		}

		return;
	} // end of main method

	// Note: the valid inputs for this program is one of:
	//	+ x y
	//	- x y
	// 	q
	//	Q
	// where x and y are two integer numbers separated by a space bar. There's no extra sanity check codes so the wrong input can crash the program.

	// our event handlers (which in a procedural programming sense is a helper method for the main event loop)
	private static void addPrintNumbers(int x, int y)
	{
		// add the two numbers then print it out to console
		System.out.println(x + y);
	}

	private static void subPrintNumbers(int x, int y)
	{
		System.out.println(x - y);
	}
}
