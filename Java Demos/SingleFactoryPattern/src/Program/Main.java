package Program;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		ArrayList<Shape> myShapes = new ArrayList<Shape>();
		
		myShapes.add(ShapeFactory.makeShape("Circle"));
		myShapes.add(ShapeFactory.makeShape("Rectangle"));
		myShapes.add(ShapeFactory.makeShape("Circle"));
		myShapes.add(ShapeFactory.makeShape("Rectangle"));
		
		for(Shape s : myShapes)
		{
			System.out.println("Shape " + s.getName() + " has area " + s.getArea());
		}
	}

}
