package Program;

import Shapes.Circle;
import Shapes.Rectangle;

// The Factory is responsible for creating objects based upon dynamically available information
public class ShapeFactory {
	
	// In this case, it creates either a Circle or Square object depending on what the input string "kind" is
	// The value of the input string could be from a user provided file, an external database, or network
	// The value is not known ahead of time by the developer, but given some particular value, we create one of the objects
	// and return it
	
	// note that both Circle and Square implement the Shape interface, which is used to return a common reference to the object
	public static Shape makeShape(String kind)
	{
		switch(kind)
		{
		case "Circle":
			return new Circle(10.0d, "circle");
		case "Rectangle":
			return new Rectangle(5.0d, 6.0d, "rectangle");
		default:
			return null;
		}
		
	}
}
